require File.expand_path('config/environment')
require 'inventory'
require 'location'
require 'item'
require 'color'
require 'lot'
require 'fabric'

@fabric = Fabric.all

@fabric.each do |f|
    @inventory = Inventory.where(item_id: f.item_id)
    @inventory.each do |i|
        if(i.color_id == f.color_id)
            i.update(fabric_id: f.id)
        end
    end
end