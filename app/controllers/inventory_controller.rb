class InventoryController < ApplicationController
  
  def index
    @inventory = Inventory.includes(:location,:lot,:history,fabric: [:item,:color]).all.where.not(current_yard: 0)
  end
  
  def sold_inventory
    @sold_inventory = Inventory.includes(:location,:lot,:history,fabric: [:item,:color]).all.where(current_yard: 0)
  end
  
  def out_of_stock
    @id = []
    @fabric_sum = Inventory.group(:fabric_id).sum(:current_yard)
    @fabric_sum.each do |f|
      if(f[1] == 0)
        @id.push(f[0])
      end
    end
    @out_of_stock = Fabric.includes(:color,:item).where(id: @id)
  end
  
end
