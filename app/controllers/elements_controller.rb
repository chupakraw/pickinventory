class ElementsController < ApplicationController
    
  def lots
    @lots = Lot.all
  end
  
  def items
    @items = Item.all
  end
  
  def colors
    @colors = Color.all
  end
  
  def locations
    @locations = Location.all
  end
  
  def fabrics
    @fabrics = Fabric.all
  end
    
end
