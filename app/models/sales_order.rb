class SalesOrder < ApplicationRecord
    
    has_many :histories
    has_many :sales_order_histories
    belongs_to :customer
    
end
