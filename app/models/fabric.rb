class Fabric < ApplicationRecord
  belongs_to :color
  belongs_to :item
  has_many :inventories
end