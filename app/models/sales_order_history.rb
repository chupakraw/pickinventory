class SalesOrderHistory < ApplicationRecord
  has_many :daily_picklists
  has_many :warehouse_pullsheets
  
  belongs_to :sales_order
  belongs_to :fabric
end
