class HistoryNote < ApplicationRecord
    
    has_many :histories
    
end
