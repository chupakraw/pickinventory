class History < ApplicationRecord
  has_many :daily_picklists
  has_many :warehouse_pullsheets
  
  belongs_to :inventories
  belongs_to :history_category
  belongs_to :sales_order
  belongs_to :history_note
end
