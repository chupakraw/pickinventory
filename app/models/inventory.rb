class Inventory < ApplicationRecord
  has_many :history
  
  belongs_to :lot
  belongs_to :location
  belongs_to :fabric
  validates :fabric_id, presence: true
end
