class CreateWarehousePullsheets < ActiveRecord::Migration[5.0]
  def change
    create_table :warehouse_pullsheets do |t|
      t.references :sales_order_history, foreign_key: true
      t.references :history, foreign_key: true

      t.timestamps
    end
  end
end
