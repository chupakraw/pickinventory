class AddInventoryIdToHistory < ActiveRecord::Migration[5.0]
  def change
    add_reference :histories, :inventory, foreign_key: true
  end
end
