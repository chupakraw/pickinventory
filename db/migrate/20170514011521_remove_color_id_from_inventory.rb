class RemoveColorIdFromInventory < ActiveRecord::Migration[5.0]
  def change
    remove_reference :inventories, :color, foreign_key: true
  end
end
