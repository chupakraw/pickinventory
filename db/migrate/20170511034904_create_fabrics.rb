class CreateFabrics < ActiveRecord::Migration[5.0]
  def change
    create_table :fabrics do |t|
      t.references :item, foreign_key: true
      t.references :color, foreign_key: true
      t.string :item_color_string

      t.timestamps
    end
  end
end
