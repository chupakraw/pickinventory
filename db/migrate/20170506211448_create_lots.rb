class CreateLots < ActiveRecord::Migration[5.0]
  def change
    create_table :lots do |t|
      t.string :lot
      t.string :invoice
      t.date :date_into_warehouse

      t.timestamps
    end
  end
end
