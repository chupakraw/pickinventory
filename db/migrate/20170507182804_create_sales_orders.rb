class CreateSalesOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :sales_orders do |t|
      t.string :so_number
      t.string :customer
      t.date :date
      t.date :ship_date

      t.timestamps
    end
  end
end
