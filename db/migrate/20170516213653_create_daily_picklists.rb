class CreateDailyPicklists < ActiveRecord::Migration[5.0]
  def change
    create_table :daily_picklists do |t|
      t.references :sales_order_history, foreign_key: true
      t.references :history, foreign_key: true

      t.timestamps
    end
  end
end
