class AddFabricToInventory < ActiveRecord::Migration[5.0]
  def change
    add_reference :inventories, :fabric, foreign_key: true
  end
end
