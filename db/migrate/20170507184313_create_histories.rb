class CreateHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :histories do |t|
      t.date :date
      t.references :history_category, foreign_key: true
      t.references :sales_order, foreign_key: true
      t.string :location_before
      t.string :location_after
      t.integer :yard_before
      t.integer :yard_after
      t.integer :yard_difference
      t.references :history_note, foreign_key: true

      t.timestamps
    end
  end
end
