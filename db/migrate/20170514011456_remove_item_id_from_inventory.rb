class RemoveItemIdFromInventory < ActiveRecord::Migration[5.0]
  def change
    remove_reference :inventories, :item, foreign_key: true
  end
end
