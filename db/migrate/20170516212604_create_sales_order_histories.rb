class CreateSalesOrderHistories < ActiveRecord::Migration[5.0]
  def change
    create_table :sales_order_histories do |t|
      t.references :sales_order, foreign_key: true
      t.references :fabric, foreign_key: true
      t.integer :yard

      t.timestamps
    end
  end
end
