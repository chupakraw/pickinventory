class AddCustomerIdToSalesOrder < ActiveRecord::Migration[5.0]
  def change
    add_reference :sales_orders, :customer, foreign_key: true
  end
end
