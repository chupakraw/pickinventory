class CreateInventories < ActiveRecord::Migration[5.0]
  def change
    create_table :inventories do |t|
      t.integer :roll
      t.integer :kg
      t.integer :factory_meter
      t.integer :factory_yard
      t.integer :current_yard
      t.integer :flaws
      t.date :date_out_warehouse
      t.string :remark
      t.boolean :hold
      t.references :lot, foreign_key: true
      t.references :item, foreign_key: true
      t.references :location, foreign_key: true
      t.references :color, foreign_key: true
      t.references :history, foreign_key: true

      t.timestamps
    end
  end
end
