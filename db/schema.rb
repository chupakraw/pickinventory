# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170516213724) do

  create_table "colors", force: :cascade do |t|
    t.string   "color"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "customers", force: :cascade do |t|
    t.string   "customer"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "daily_picklists", force: :cascade do |t|
    t.integer  "sales_order_history_id"
    t.integer  "history_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["history_id"], name: "index_daily_picklists_on_history_id"
    t.index ["sales_order_history_id"], name: "index_daily_picklists_on_sales_order_history_id"
  end

  create_table "fabrics", force: :cascade do |t|
    t.integer  "item_id"
    t.integer  "color_id"
    t.string   "item_color_string"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.index ["color_id"], name: "index_fabrics_on_color_id"
    t.index ["item_id"], name: "index_fabrics_on_item_id"
  end

  create_table "histories", force: :cascade do |t|
    t.date     "date"
    t.integer  "history_category_id"
    t.integer  "sales_order_id"
    t.string   "location_before"
    t.string   "location_after"
    t.integer  "yard_before"
    t.integer  "yard_after"
    t.integer  "yard_difference"
    t.integer  "history_note_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "inventory_id"
    t.index ["history_category_id"], name: "index_histories_on_history_category_id"
    t.index ["history_note_id"], name: "index_histories_on_history_note_id"
    t.index ["inventory_id"], name: "index_histories_on_inventory_id"
    t.index ["sales_order_id"], name: "index_histories_on_sales_order_id"
  end

  create_table "history_categories", force: :cascade do |t|
    t.string   "category"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "history_notes", force: :cascade do |t|
    t.string   "note"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "inventories", force: :cascade do |t|
    t.integer  "roll"
    t.integer  "kg"
    t.integer  "factory_meter"
    t.integer  "factory_yard"
    t.integer  "current_yard"
    t.integer  "flaws"
    t.date     "date_out_warehouse"
    t.string   "remark"
    t.boolean  "hold"
    t.integer  "lot_id"
    t.integer  "location_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "fabric_id"
    t.index ["fabric_id"], name: "index_inventories_on_fabric_id"
    t.index ["location_id"], name: "index_inventories_on_location_id"
    t.index ["lot_id"], name: "index_inventories_on_lot_id"
  end

  create_table "items", force: :cascade do |t|
    t.string   "item"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "location"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "lots", force: :cascade do |t|
    t.string   "lot"
    t.string   "invoice"
    t.date     "date_into_warehouse"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "sales_order_histories", force: :cascade do |t|
    t.integer  "sales_order_id"
    t.integer  "fabric_id"
    t.integer  "yard"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["fabric_id"], name: "index_sales_order_histories_on_fabric_id"
    t.index ["sales_order_id"], name: "index_sales_order_histories_on_sales_order_id"
  end

  create_table "sales_orders", force: :cascade do |t|
    t.string   "so_number"
    t.string   "customer"
    t.date     "date"
    t.date     "ship_date"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "customer_id"
    t.index ["customer_id"], name: "index_sales_orders_on_customer_id"
  end

  create_table "warehouse_pullsheets", force: :cascade do |t|
    t.integer  "sales_order_history_id"
    t.integer  "history_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["history_id"], name: "index_warehouse_pullsheets_on_history_id"
    t.index ["sales_order_history_id"], name: "index_warehouse_pullsheets_on_sales_order_history_id"
  end

end
