require 'test_helper'

class SalesOrderControllerTest < ActionDispatch::IntegrationTest
  test "should get sales_order" do
    get sales_order_sales_order_url
    assert_response :success
  end

  test "should get current_orders" do
    get sales_order_current_orders_url
    assert_response :success
  end

  test "should get sales_order_history" do
    get sales_order_sales_order_history_url
    assert_response :success
  end

end
