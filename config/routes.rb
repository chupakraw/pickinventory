Rails.application.routes.draw do
  
  get 'sales_order/sales_order'

  get 'sales_order/current_orders'

  get 'sales_order/sales_order_history'

  get 'inventory/index'
  get '/sold', to: 'inventory#sold_inventory'
  get '/out_of_stock', to: 'inventory#out_of_stock'
  
  get '/lots', to: "elements#lots"
  get '/colors', to: "elements#colors"
  get '/items', to: "elements#items"
  get '/locations', to: "elements#locations"
  get '/fabrics', to: "elements#fabrics"
  
  
  root 'inventory#index'
  
end
